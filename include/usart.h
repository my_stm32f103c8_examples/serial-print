#ifndef __UART_H__
#define __UART_H__

#include "main.h"

#define USART1_RX GPIO_PIN_10
#define USART1_TX GPIO_PIN_9
#define USART1_Port GPIOA

void USART1_UART_Init(void);

#endif /* ifndef __UART_H__ */
