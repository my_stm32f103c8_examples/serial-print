#include "usart.h"

void USART1_UART_Init(void) {
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;

  HAL_UART_MspInit(&huart1);
}

void HAL_UART_MspInit(UART_HandleTypeDef* uartHandle) {
  GPIO_InitTypeDef GPIO_InitStuct = {0};

  __HAL_RCC_USART1_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  GPIO_InitStuct.Pin = USART1_TX;
  GPIO_InitStuct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStuct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(USART1_Port, &GPIO_InitStuct);

  GPIO_InitStuct.Pin = USART1_RX;
  GPIO_InitStuct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStuct.Pull = GPIO_NOPULL;
}

